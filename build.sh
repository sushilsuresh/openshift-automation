#!/usr/bin/env bash
set -euo pipefail

# Use vfs with buildah. Docker offers overlayfs as a default, but buildah
# cannot stack overlayfs on top of another overlayfs filesystem.
export STORAGE_DRIVER=vfs

# Write all image metadata in the docker format, not the standard OCI format.
# Newer versions of docker can handle the OCI format, but older versions, like
# the one shipped with Fedora 30, cannot handle the format.
export BUILDAH_FORMAT=docker

# Log into GitLab's container repository.
export REGISTRY_AUTH_FILE=${HOME}/auth.json
echo "$CI_REGISTRY_PASSWORD" | buildah login -u "$CI_REGISTRY_USER" --password-stdin $CI_REGISTRY


OCP_VERSION="latest"
if [ $# -ge 1 ]
then
    if [ "${1}" == "master" ]
    then
        OCP_VERSION="latest"
    else
        OCP_VERSION=${1}
    fi
fi

# Set up the container's fully qualified name.
FQ_IMAGE_NAME="${CI_REGISTRY_IMAGE}:${OCP_VERSION}"

# Build the container image`
echo "Bulding openshift-auomation container image using buildah."
buildah bud -f builds/openshift-automation -t openshift-automation .
CONTAINER_ID=$(buildah from openshift-automation)

echo "buildah commit $CONTAINER_ID $FQ_IMAGE_NAME"
buildah commit $CONTAINER_ID $FQ_IMAGE_NAME

# Push the container image to gitlab image registry
echo "buildah push ${FQ_IMAGE_NAME}"
buildah push ${FQ_IMAGE_NAME}
