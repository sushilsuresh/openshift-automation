#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset
# set -o xtrace


# RELEASE PAGE - https://mirror.openshift.com/pub/openshift-v4/clients/ocp/
# Setting the OCP_VERSION
# If none is provided we go with latest, if not we go with the branch name
OCP_VERSION="latest"
if [ $# -ge 1 ]
then
    if [ "${1}" == "master" ]
    then
        OCP_VERSION="latest"
    else
        OCP_VERSION=${1}
    fi
fi
OCP_CLIENT_URL="https://mirror.openshift.com/pub/openshift-v4/clients/ocp/${OCP_VERSION}/openshift-client-linux.tar.gz"
OCP_INSTALLER_URL="https://mirror.openshift.com/pub/openshift-v4/clients/ocp/${OCP_VERSION}/openshift-install-linux.tar.gz"

# RELEASE PAGE - https://github.com/vmware/govmomi/releases
GOVC_VERSION="v0.22.1"
GOVC_URL="https://github.com/vmware/govmomi/releases/download/${GOVC_VERSION}/govc_linux_amd64.gz"

# RELEASE PAGE - https://github.com/hashicorp/terraform/releases
# NOTE: please DO NOT include the "v" in the release "v0.12.24"
TERRAFORM_VERSION="0.12.24"
TERRAFORM_URL="https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip"

# RELEASE PAGE - https://github.com/derailed/k9s/releases
K9S_VERSION="v0.19.4"
K9S_URL="https://github.com/derailed/k9s/releases/download/${K9S_VERSION}/k9s_Linux_x86_64.tar.gz"

# Set magic variables for current file & dir
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__file="${__dir}/$(basename "${BASH_SOURCE[0]}")"
BIN_DIR="${__dir}/bin"

cd ${BIN_DIR}

echo "Downloading govc version = ${GOVC_VERSION}"
wget -q ${GOVC_URL} -O govc.gz
gunzip govc.gz

echo "Downloading terraform version = ${TERRAFORM_VERSION}"
wget -q ${TERRAFORM_URL} -O terraform.zip
unzip -qq terraform.zip
rm terraform.zip

echo "Downloading k9s version = ${K9S_VERSION}"
wget -q ${K9S_URL} -O k9s.tar.gz
tar -xzf k9s.tar.gz k9s
rm k9s.tar.gz

echo "Downloading openshift-client version = ${OCP_VERSION}"
wget -q ${OCP_CLIENT_URL} -O openshift-client.tar.gz
tar -xzf openshift-client.tar.gz oc kubectl
rm openshift-client.tar.gz

echo "Downloading openshift-install version = ${OCP_VERSION}"
wget -q ${OCP_INSTALLER_URL} -O openshift-install.tar.gz
tar -xzf openshift-install.tar.gz openshift-install
rm openshift-install.tar.gz

chmod +x *
